FROM openjdk:11-jdk

ENV NODE_VERSION=14
ENV NVM_DIR=/root/.nvm
RUN apt update && apt install -y curl \
&& curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash \
&& . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} \
&& . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION} \
&& . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION} 

ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"


